/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo4;

import static es.uja.ssccdd.curso2021.problemassesion8.grupo4.Utils.REPARTIDORES_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo4.Utils.TIEMPO_ESPERA_HILO_PRINCIPAL;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo4.Utils.TAMAÑO_COLA_MENUS;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo4.Utils.CLIENTES_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo4.Utils.RESTAURANTES_A_GENERAR;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion8 {

    public static void main(String[] args) {
        // Variables aplicación
        int idClientes = 0;
        int idRepartidores = 0;
        int idRestaurantes = 0;

        BlockingDeque<MenuReparto> listaDeMenus = new LinkedBlockingDeque<>(TAMAÑO_COLA_MENUS);
        DelayQueue<Plato> listaDePlatos = new DelayQueue<>();
        ExecutorService executor = (ExecutorService) Executors.newCachedThreadPool();

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        System.out.println("HILO-Principal Generando restaurantes");
        for (int i = 0; i < RESTAURANTES_A_GENERAR; i++) {
            executor.execute(new Restaurante(i, listaDePlatos));
        }

        System.out.println("HILO-Principal Generando clientes");
        for (int i = 0; i < CLIENTES_A_GENERAR; i++) {
            executor.execute(new Cliente(i, listaDeMenus));
        }

        System.out.println("HILO-Principal Generando repartidores");
        for (int i = 0; i < REPARTIDORES_A_GENERAR; i++) {

            // Inicializamos los repartidores
            executor.execute(new Repartidor(i, listaDeMenus, listaDePlatos));

        }

        System.out.println("HILO-Principal Espera para parar a los repartidores");

        try {
            TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_HILO_PRINCIPAL);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion8.class.getName()).log(Level.SEVERE, null, ex);
        }

        executor.shutdownNow();

        try {
            executor.awaitTermination(TIEMPO_ESPERA_HILO_PRINCIPAL, TimeUnit.DAYS);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion8.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Ha finalizado la ejecución");

        System.out.println("\n\nHILO-Principal Listado " + listaDePlatos.size() + " platos pendientes.");
        while (!listaDePlatos.isEmpty()) {
            System.out.println(listaDePlatos.poll());
        }

        System.out.println("\n\nHILO-Principal Listado " + listaDeMenus.size() + " menús pendientes.");
        while (!listaDeMenus.isEmpty()) {
            System.out.println(listaDeMenus.poll());
        }
    }

}
