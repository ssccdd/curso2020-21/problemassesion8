/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo6;

import static es.uja.ssccdd.curso2021.problemassesion8.grupo6.Utils.VALOR_GENERACION;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo6.Utils.FabricanteVacuna;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo6.Utils.random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.BlockingDeque;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class AlmacenMedico implements Runnable {

    private final int iD;
    private final BlockingDeque<DosisVacuna> dosis;
    private static int siguienteID = 0;
    private static ReentrantLock lockID = new ReentrantLock();

    public AlmacenMedico(int iD, BlockingDeque<DosisVacuna> dosis) {
        this.iD = iD;
        this.dosis = dosis;
    }

    private static int getSiguienteID() {
        int id = 0;
        lockID.lock();
        id = siguienteID++;
        lockID.unlock();
        return id;
    }

    @Override
    public void run() {

        boolean interrumpido = false;
        int contadorPacientes = 0;
        System.out.println("Almacén " + iD + " ha empezado.");

        while (!interrumpido) {

            try {

                FabricanteVacuna fabricante = FabricanteVacuna.getFabricanteAleatorio(random.nextInt(VALOR_GENERACION));
                dosis.put(new DosisVacuna(getSiguienteID(), fabricante));
                contadorPacientes++;

                TimeUnit.MILLISECONDS.sleep(Utils.TIEMPO_ESPERA_ALMACEN);
            } catch (InterruptedException ex) {
                interrumpido = true;
            }

        }

        System.out.println("El almacén médico " + iD + " ha terminado el trabajo.\tTotal dosis generadas: " + contadorPacientes);
    }

}
