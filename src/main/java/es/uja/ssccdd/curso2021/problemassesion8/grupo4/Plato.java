/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo4;

import es.uja.ssccdd.curso2021.problemassesion8.grupo4.Utils.TipoPlato;
import java.util.Date;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Plato implements Delayed {

    private final int iD;
    private final TipoPlato tipo;
    private final Date horaListo;

    public Plato(int iD, TipoPlato plato, Date horaListo) {
        this.iD = iD;
        this.tipo = plato;
        this.horaListo = horaListo;
    }

    public int getiD() {
        return iD;
    }

    public TipoPlato getTipo() {
        return tipo;
    }

    /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        Date now = new Date();
        long diff = horaListo.getTime() - now.getTime();
        if (diff > 0) {
            return "Plato{" + "iD= " + iD + ", tipo de plato= " + tipo + ", le quedan " + diff + " milisegundos}";
        } else {
            return "Plato{" + "iD= " + iD + ", tipo de plato= " + tipo + ", listo}";
        }
    }

    /**
     * Method to compare two events
     */
    @Override
    public int compareTo(Delayed o) {
        long result = this.getDelay(TimeUnit.NANOSECONDS) - o.getDelay(TimeUnit.NANOSECONDS);
        if (result < 0) {
            return -1;
        } else if (result > 0) {
            return 1;
        }
        return 0;
    }

    /**
     * Method that returns the remaining time to the activation of the event
     */
    @Override
    public long getDelay(TimeUnit unit) {
        Date now = new Date();
        long diff = horaListo.getTime() - now.getTime();
        return unit.convert(diff, TimeUnit.MILLISECONDS);
    }

}
