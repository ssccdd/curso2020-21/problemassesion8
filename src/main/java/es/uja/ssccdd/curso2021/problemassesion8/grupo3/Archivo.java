/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo3;

import static es.uja.ssccdd.curso2021.problemassesion8.grupo3.Constantes.MINIMO;
import es.uja.ssccdd.curso2021.problemassesion8.grupo3.Constantes.TipoArchivo;
import java.util.Date;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Archivo implements Delayed {
    private final int iD;
    private final TipoArchivo tipoArchivo;
    private final Date disponible;
    
    // Constantes
    private final static int MENOR = -1;
    private final static int IGUAL = 0;
    private final static int MAYOR = 1;


    public Archivo(int iD, TipoArchivo tipoArchivo) {
        this.iD = iD;
        this.tipoArchivo = tipoArchivo;
        this.disponible = new Date();
        
        // La disponibilidad la vinculamos con el tipo de archivo
        long tiempo = MINIMO + tipoArchivo.ordinal();
        this.disponible.setTime(this.disponible.getTime() +
                                TimeUnit.MILLISECONDS.convert(tiempo, TimeUnit.SECONDS));
    }

    public int getiD() {
        return iD;
    }

    public TipoArchivo getTipoArchivo() {
        return tipoArchivo;
    }

    @Override
    public String toString() {
        return "Archivo{" + "iD=" + iD + ", tipoArchivo=" + tipoArchivo + '}';
    }

    @Override
    public long getDelay(TimeUnit unit) {
        // Tiempo que resta para la activación del archivo
        Date actual = new Date();
        long diferencia = disponible.getTime() - actual.getTime();
        return unit.convert(diferencia, TimeUnit.MILLISECONDS);
    }

    @Override
    public int compareTo(Delayed o) {
        int resultado;
        
        // Comparamos los tiempos de retardo de dos archivos
        long diferencia = this.getDelay(TimeUnit.NANOSECONDS) - o.getDelay(TimeUnit.NANOSECONDS);
        
        if ( diferencia < 0 )
            resultado = MENOR;
        else if ( diferencia > 0 )
            resultado = MAYOR;
        else 
            resultado = IGUAL;
        
        return resultado;
    }
}
