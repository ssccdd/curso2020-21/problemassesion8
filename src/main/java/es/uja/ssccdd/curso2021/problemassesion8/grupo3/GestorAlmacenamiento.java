/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo3;

import static es.uja.ssccdd.curso2021.problemassesion8.grupo3.Constantes.ESPACIO_INSUFICIENTE;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo3.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo3.Constantes.MAXIMO_ALMACENAMIENTO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo3.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo3.Constantes.MINIMO_ALMACENAMIENTO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo3.Constantes.NUM_ARCHIVOS;
import es.uja.ssccdd.curso2021.problemassesion8.grupo3.Constantes.TipoArchivo;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo3.Constantes.aleatorio;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo3.Sesion8.generaID_SISTEMA;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class GestorAlmacenamiento implements Callable<Informe> {
    private final String iD;
    private final BlockingQueue<Archivo> archivosCreados;
    private final ArrayList<UnidadAlmacenamiento> listaDeAlmacenamiento;
    private final ArrayList<Archivo> noAsignados;
    private int archivosProcesados;

    public GestorAlmacenamiento(String iD, BlockingQueue<Archivo> archivosCreados) {
        this.iD = iD;
        this.archivosCreados = archivosCreados;
        this.listaDeAlmacenamiento = new ArrayList();
        this.noAsignados = new ArrayList();
        this.archivosProcesados = 0;
    }

    @Override
    public Informe call() throws Exception {
        Informe informe = new Informe(iD);
        
        System.out.println("TAREA-" + iD + " Empieza su ejecución");
        
        inicializaGestor();
        
        try {
            // Hasta que se alcance el número de fallos o sea interrumpido
            while( archivosProcesados < NUM_ARCHIVOS ) {
                
                asignarArchivos();
                
                archivosProcesados++;
            }
            
            System.out.println("Ha finalizado la ejecución del TAREA-" + iD);
        } catch (InterruptedException ex) {
            System.out.println("Se ha interrumpido la ejecución del TAREA-" + iD + ex);
        } finally {
            // Completa la finbalización del gestor
            finalizacion(informe);
        }
        
        return informe;
    }
    
    private void inicializaGestor() {
        int capacidad;
        
        int totalUnidadesAlmacenamiento = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
        for(int j = 0; j < totalUnidadesAlmacenamiento; j++) {
            // Generamos la capacidad de la unidad de almacenamiento
            capacidad = MINIMO_ALMACENAMIENTO + aleatorio.nextInt(MAXIMO_ALMACENAMIENTO - 
                                                                      MINIMO_ALMACENAMIENTO + 1);
            
            listaDeAlmacenamiento.add(new UnidadAlmacenamiento(generaID_SISTEMA(),capacidad));
        }
    }
    
    private void asignarArchivos() throws InterruptedException {
        // Comprueba la solicitud de interrupción de la tarea antes de comenzar
        // con la siguiente asignación de proceso.
        if ( Thread.interrupted() )
           throw new InterruptedException();
            
        Archivo archivo = archivosCreados.take();
        System.out.println("TAREA-" + iD + " Asignando " + archivo);
                
        // Asignamos el archivo a una unidad de almacenamiento
        boolean asignado = false;
        int indice = 0;
        while ((indice < listaDeAlmacenamiento.size()) && !asignado) {
            if (listaDeAlmacenamiento.get(indice).addArchivo(archivo) != ESPACIO_INSUFICIENTE) {
                asignado = true;
            } else 
               indice++;
        }

        // Si no se ha podido almacenar el archivo se anota
        if(!asignado) {
            noAsignados.add(archivo);
        }

        // Simulamos por último el tiempo de asignación porque si se interrumpe
        // ya hemos completado la operación de asignación
        TimeUnit.SECONDS.sleep(tiempoAsignacion(archivo.getTipoArchivo()));
    }
    
    private void finalizacion(Informe informe) {
        informe.setListaDeAlmacenamiento(listaDeAlmacenamiento);
        informe.setListaNoAsignados(noAsignados);
    }
    
    private int tiempoAsignacion(TipoArchivo tipoArchivo) {
        return MAXIMO + tipoArchivo.ordinal();
    }
}
