/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo1;

import static es.uja.ssccdd.curso2021.problemassesion8.grupo1.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo1.Constantes.MEMORIA_COMPLETA;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo1.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo1.Constantes.NUM_FALLOS;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo1.Constantes.TIPOS_PROCESO;
import es.uja.ssccdd.curso2021.problemassesion8.grupo1.Constantes.TipoProceso;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo1.Constantes.aleatorio;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo1.Sesion8.generaID_SISTEMA;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class GestorMemoria implements Callable<Informe> {
    private final String iD;
    private final BlockingQueue<Proceso> procesosCreados;
    private final ArrayList<Ordenador> listaOrdenadores;
    private final ArrayList<Proceso> noAsignados;
    private int ultimoOrdenador; // último ordenador donde se asignó un proceso
    private int fallosAsignacion;

    public GestorMemoria(String iD, BlockingQueue<Proceso> procesosCreados) {
        this.iD = iD;
        this.procesosCreados = procesosCreados;
        this.listaOrdenadores = new ArrayList();
        this.noAsignados = new ArrayList();
        this.ultimoOrdenador = 0;
        this.fallosAsignacion = 0;
    }

    

    @Override
    public Informe call() throws Exception {
        Informe informe = new Informe(iD);
        
        System.out.println("TAREA-" + iD + " Empieza su ejecución");
        
        inicializaGestor();
        
        try {
            // Hasta que se alcance el número de fallos o sea interrumpido
            while( fallosAsignacion < NUM_FALLOS ) {
                
                asignarProcesos();
            }
            
            System.out.println("Ha finalizado la ejecución del TAREA-" + iD);
        } catch (InterruptedException ex) {
            System.out.println("Se ha interrumpido la ejecución del TAREA-" + iD + ex);
        } finally {
            // Completa la finbalización del gestor
            finalizacion(informe);
        }
        
        return informe;
    }

    private void inicializaGestor() {
        int[] capacidad = new int[TIPOS_PROCESO];
        
        int totalOrdenadores = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
        for(int j = 0; j < totalOrdenadores; j++) {
            // Generamos la capacidad del ordenador
            for(int k = 0; k < TIPOS_PROCESO; k++) 
                capacidad[k] = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
            
            listaOrdenadores.add(new Ordenador(generaID_SISTEMA(),capacidad));
        }
    }
    
    private void asignarProcesos() throws InterruptedException {
        // Asignamos los procesos a los ordenadores
        if ( Thread.interrupted() )
            throw new InterruptedException();
            
        Proceso proceso = procesosCreados.take();
        System.out.println("TAREA-" + iD + " Asignando " + proceso);
                
        // Asignamos el proceso a un ordenador si es posible
        boolean asignado = false;
        int ordenadoresRevisados = 0;
        while( (ordenadoresRevisados < listaOrdenadores.size()) && !asignado ) {
        int indice = (ultimoOrdenador + ordenadoresRevisados) % listaOrdenadores.size();
        if(listaOrdenadores.get(indice).addProceso(proceso) != MEMORIA_COMPLETA)
            asignado = true;
        else
            ordenadoresRevisados++;
        }
            
        // Si no se ha asignado se anota el proceso en otro caso se actualiza
        // el último ordenador asignado
        if(asignado)
            ultimoOrdenador = (ultimoOrdenador + ordenadoresRevisados + 1) % listaOrdenadores.size();
        else {
            noAsignados.add(proceso);
            fallosAsignacion++;
        }
            
        // Simulamos por último el tiempo de asignación porque si se interrumpe
        // ya hemos completado la operación de asignación
        TimeUnit.SECONDS.sleep(tiempoAsignacion(proceso.getTipoProceso()));
    }
    
    private void finalizacion(Informe informe) {
        // registramos los procesos no asignados
        informe.setListaOrdenadores(listaOrdenadores);
        informe.setListaNoAsignados(noAsignados);
    }
    
    public String getiD() {
        return iD;
    }

    private int tiempoAsignacion(TipoProceso tipoProceso) {
        return MAXIMO + tipoProceso.ordinal();
    }
}
