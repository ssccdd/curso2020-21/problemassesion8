/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo5;

import static es.uja.ssccdd.curso2021.problemassesion8.grupo5.Utils.TIEMPO_ESPERA_MAQUINA;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class MaquinaPostProcesado implements Runnable {

    private final int iD;
    private final DelayQueue<Modelo> modelosImpresos;
    private final BlockingDeque<Modelo> modelosPostprocesados;

    public MaquinaPostProcesado(int iD, DelayQueue<Modelo> modelosImpresos, BlockingDeque<Modelo> modelosPostprocesados) {
        this.iD = iD;
        this.modelosImpresos = modelosImpresos;
        this.modelosPostprocesados = modelosPostprocesados;
    }

    @Override
    public void run() {

        int modelosProcesados = 0;
        boolean interrumpido = false;

        System.out.println("Maquina de postprocesado " + iD + " iniciada");

        while (!interrumpido) {

            try {

                Modelo modeloAProcesar = modelosImpresos.take();

                modelosPostprocesados.put(modeloAProcesar);

                modelosProcesados++;

                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_MAQUINA);
            } catch (InterruptedException ex) {
                interrumpido = true;
            }
        }

        System.out.println("La máquina " + iD + " ha terminado el trabajo.\tTotal modelos procesados: " + modelosProcesados);

    }

}
