/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo5;

import static es.uja.ssccdd.curso2021.problemassesion8.grupo5.Utils.CalidadImpresion;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo5.Utils.TIEMPO_ESPERA_CLIENTE;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Cliente implements Runnable {

    private final int iD;
    private final BlockingDeque<Modelo> listaDePeticiones;
    private final CalidadImpresion calidad;
    private static int siguienteID = 0;
    private static ReentrantLock lockID = new ReentrantLock();

    public Cliente(int iD, BlockingDeque<Modelo> listaDePeticiones, CalidadImpresion calidad) {
        this.iD = iD;
        this.listaDePeticiones = listaDePeticiones;
        this.calidad = calidad;
    }

    private static int getSiguienteID() {
        int id = 0;
        lockID.lock();
        id = siguienteID++;
        lockID.unlock();
        return id;
    }

    @Override
    public void run() {

        int contadorPeticiones = 0;
        System.out.println("Cliente " + iD + " ha empezado.");

        try {

            while (true) {

                listaDePeticiones.put(new Modelo(getSiguienteID(), calidad));
                contadorPeticiones++;
                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_CLIENTE);

            }

        } catch (InterruptedException ex) {
            //Si nos interrumpen no tenemos que modificar nada.
        }

        System.out.println("Cliente " + iD + " ha terminado por interrupción.\tTotal modelos pedidos: " + contadorPeticiones);
        
    }

}
