/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo5;

import java.util.Random;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Utils {

    public static Random random = new Random();

    // Constantes del problema
    public static final int VALOR_GENERACION = 101; // Valor máximo
    public static final int TOTAL_CALIDADES = CalidadImpresion.values().length;
    public static final int TIEMPO_ESPERA_HILO_PRINCIPAL = 15000;
    public static final int TAMAÑO_COLA_PETICIONES = 20;
    public static final int DELAY_MODELOS_MINIMO = 3;
    public static final int DELAY_MODELOS_MAXIMO = 9;
    public static final int TIEMPO_ESPERA_CLIENTE = 500;
    public static final int TIEMPO_ESPERA_INGENIERO = 450;
    public static final int TIEMPO_ESPERA_MAQUINA = 350;
    public static final int CLIENTES_A_GENERAR = 15;
    public static final int INGENIEROS_A_GENERAR = 10;
    public static final int MAQUINAS_A_GENERAR = 5;

    //Enumerado para el tipo de calidad de impresión
    public enum CalidadImpresion {
        MEDICINA(25), TALLER(75), INDUSTRIAL(100);

        private final int valor;

        private CalidadImpresion(int valor) {
            this.valor = valor;
        }

        /**
         * Obtenemos un calidad de impresión relacionada con su valor de
         * generación
         *
         * @param valor, entre 0 y 100, de generación de calidad
         * @return la CalidadImpresión con el valor de generación
         */
        public static CalidadImpresion getCalidad(int valor) {
            CalidadImpresion resultado = null;
            CalidadImpresion[] calidades = CalidadImpresion.values();
            int i = 0;

            while ((i < calidades.length) && (resultado == null)) {
                if (calidades[i].valor >= valor) {
                    resultado = calidades[i];
                }

                i++;
            }

            return resultado;
        }
    }
}
