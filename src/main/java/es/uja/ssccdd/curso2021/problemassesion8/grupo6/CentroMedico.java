/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo6;

import static es.uja.ssccdd.curso2021.problemassesion8.grupo6.Utils.TIEMPO_ESPERA_CENTRO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo6.Utils.PACIENTE_EDAD_MINIMA;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo6.Utils.PACIENTE_EDAD_MAXIMA;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo6.Utils.random;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class CentroMedico implements Runnable {

    private final int iD;
    private final PriorityBlockingQueue<Paciente> pacientes;
    private static int siguienteID = 0;
    private static ReentrantLock lockID = new ReentrantLock();

    public CentroMedico(int iD, PriorityBlockingQueue<Paciente> pacientes) {
        this.iD = iD;
        this.pacientes = pacientes;
    }

    private static int getSiguienteID() {
        int id = 0;
        lockID.lock();
        id = siguienteID++;
        lockID.unlock();
        return id;
    }

    @Override
    public void run() {

        boolean interrumpido = false;
        int contadorPacientes = 0;

        System.out.println("Centro " + iD + " ha empezado.");

        while (!interrumpido) {

            try {

                int edad = random.nextInt(PACIENTE_EDAD_MAXIMA - PACIENTE_EDAD_MINIMA) + PACIENTE_EDAD_MINIMA;
                pacientes.add(new Paciente(getSiguienteID(), edad));
                contadorPacientes++;

                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_CENTRO);
            } catch (InterruptedException ex) {
                interrumpido = true;
            }

        }

        System.out.println("El centro médico " + iD + " ha terminado el trabajo.\tTotal pacientes derivados: " + contadorPacientes);
    }

}
