/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo5;

import static es.uja.ssccdd.curso2021.problemassesion8.grupo5.Utils.random;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo5.Utils.DELAY_MODELOS_MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo5.Utils.DELAY_MODELOS_MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo5.Utils.TIEMPO_ESPERA_INGENIERO;
import java.util.Date;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Ingeniero implements Runnable {

    private final int iD;
    private final BlockingDeque<Modelo> listaDePeticiones;
    private final DelayQueue<Modelo> listaDeModelosImpresos;

    public Ingeniero(int iD, BlockingDeque<Modelo> listaDePeticiones, DelayQueue<Modelo> listaDeModelosImpresos) {
        this.iD = iD;
        this.listaDePeticiones = listaDePeticiones;
        this.listaDeModelosImpresos = listaDeModelosImpresos;
    }

    private static Date getRandomDelay(int minSec, int maxSec) {

        int rand = random.nextInt(maxSec - minSec) + minSec;
        Date now = new Date();
        Date delay = new Date();
        delay.setTime(now.getTime() + (rand * 1000));

        return delay;

    }

    @Override
    public void run() {

        int contadorModelosEncolados = 0;
        boolean interrumpido = false;

        System.out.println("Ingeniero " + iD + " iniciada");

        while (!interrumpido) {

            try {

                Modelo modeloAEncolar = listaDePeticiones.take();

                modeloAEncolar.setHoraTerminacion(getRandomDelay(DELAY_MODELOS_MINIMO, DELAY_MODELOS_MAXIMO));

                listaDeModelosImpresos.add(modeloAEncolar);

                contadorModelosEncolados++;

                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_INGENIERO);
            } catch (InterruptedException e) {
                interrumpido = true;
            }

        }

        System.out.println("El ingeniero " + iD + " ha terminado el trabajo.\tTotal modelos impresos: " + contadorModelosEncolados);

    }

}
