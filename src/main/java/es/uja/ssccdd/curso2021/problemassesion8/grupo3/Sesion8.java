/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo3;

import static es.uja.ssccdd.curso2021.problemassesion8.grupo3.Constantes.INICIO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo3.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo3.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo3.Constantes.NUM_GESTORES;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo3.Constantes.TIEMPO_CREAR_ARCHIVO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo3.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo3.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Sesion8 {
    static int ID_SISTEMA = 0;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ScheduledExecutorService ejecucion;
        Informe informe;
        DelayQueue<Archivo> archivosCreados;
        ArrayList<GestorAlmacenamiento> listaGestores;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicializamos las variables del sistema
        int numTareasCrearProceso = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
        ejecucion = Executors.newScheduledThreadPool(NUM_GESTORES + numTareasCrearProceso);
        listaGestores = new ArrayList();
        archivosCreados = new DelayQueue();
        
        // Creamos los gestores
        for(int i = 0; i < NUM_GESTORES; i++) {
            GestorAlmacenamiento gestor = new GestorAlmacenamiento("GESTOR(" + i + ")", archivosCreados);
            listaGestores.add(gestor);
        }
        
        // Ejecutamos la tarea para crear archivos
        // Lo modifico para crear más de un archivo y que evolucione más rápido el ejercicio
        for(int i = 0; i < numTareasCrearProceso; i++) {
            CrearArchivos tareaCrearProceso = new CrearArchivos("CREAR_PROCESO(" + i + ")", archivosCreados);
            ejecucion.scheduleAtFixedRate(tareaCrearProceso, INICIO, TIEMPO_CREAR_ARCHIVO, TimeUnit.SECONDS);
            
        }
        
        // Ejecutamos los gestores y esperamos a que finalice el primero
        System.out.println("(HILO_PRINCIPAL) Espera a la finalización de un gestor");
        informe = ejecucion.invokeAny(listaGestores);
        
        // Se solicita la cancelación de los gestores que no han terminado
        System.out.println("(HILO_PRINCIPAL) Solicita la finalización de los gestores");
        ejecucion.shutdownNow();
        ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        
        // Mostrar el estado de sistema distribuido
        System.out.println("(HILO_PRINCIPAL) Estado del gestor que ha finalizado\n" +
                            informe);
        
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
    
    public static synchronized int generaID_SISTEMA() {
        return ID_SISTEMA++;
    }
}
