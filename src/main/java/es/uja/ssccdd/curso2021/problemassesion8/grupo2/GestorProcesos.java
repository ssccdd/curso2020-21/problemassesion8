/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo2;

import static es.uja.ssccdd.curso2021.problemassesion8.grupo2.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo2.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo2.Constantes.NO_ASIGNADO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo2.Constantes.NUM_FALLOS;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo2.Constantes.TIPOS_PRIORIDAD;
import es.uja.ssccdd.curso2021.problemassesion8.grupo2.Constantes.TipoPrioridad;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo2.Constantes.aleatorio;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo2.Sesion8.generaID_SISTEMA;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class GestorProcesos implements Callable<Informe> {
    private final String iD;
    private final BlockingQueue<Proceso> procesosCreados;
    private final ArrayList<ColaPrioridad> listaColasPrioridad;
    private final ArrayList<Proceso> noAsignados;
    private int ultimaCola; // última cola de prioridad donde se asignó un proceso
    private int fallosAsignacion;

    public GestorProcesos(String iD, BlockingQueue<Proceso> procesosCreados) {
        this.iD = iD;
        this.procesosCreados = procesosCreados;
        this.listaColasPrioridad = new ArrayList();
        this.noAsignados = new ArrayList();
        this.ultimaCola = 0;
        this.fallosAsignacion = 0;
    }

    @Override
    public Informe call() throws Exception {
        Informe informe = new Informe(iD);
        
        System.out.println("TAREA-" + iD + " Empieza su ejecución");
        
        inicializaGestor();
        
        try {
            // Hasta que se alcance el número de fallos o sea interrumpido
            while( fallosAsignacion < NUM_FALLOS ) 
                asignarProcesos();
            
            System.out.println("Ha finalizado la ejecución del TAREA-" + iD);
        } catch (InterruptedException ex) {
            System.out.println("TAREA-" + iD + " ha sido CANCELADA " + ex);
        } finally {
            // Completa la finbalización del gestor
            finalizacion(informe);

        }
        
        return informe;
    }
 
    private void inicializaGestor() {
        int[] capacidad = new int[TIPOS_PRIORIDAD];
        
        int totalColasPrioridad = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
        for(int j = 0; j < totalColasPrioridad; j++) {
            // Generamos la capacidad de las diferentes prioridades en la cola
            for(int k = 0; k < TIPOS_PRIORIDAD; k++) 
                capacidad[k] = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
            
            listaColasPrioridad.add(new ColaPrioridad(generaID_SISTEMA(),capacidad));
        }
    }
    
    private void asignarProcesos() throws InterruptedException {
        int tiempo;
        Proceso proceso;
        
        // Lo primero es comprobar si se ha solicitado la interrupción de la tarea
        if ( Thread.interrupted() )
            throw new InterruptedException();
        
        // Asignamos el a un ordenador cuando haya uno disponible
        proceso = procesosCreados.take();
        System.out.println("TAREA-" + iD + " Asignando " + proceso);
        
        // Asignamos el proceso a una cola de prioridad si es posible
        boolean asignado = false;
        int colasRevisadas = 0;
        while( (colasRevisadas < listaColasPrioridad.size()) && !asignado ) {
            int indice = (ultimaCola + colasRevisadas) % listaColasPrioridad.size();
            if(listaColasPrioridad.get(indice).addProceso(proceso) != NO_ASIGNADO) {
                asignado = true;
            } else
                colasRevisadas++;
        }
            
        // Si no se ha asignado se anota el proceso en otro caso se actualiza
        // el último ordenador asignado
        if(asignado)
            ultimaCola = (ultimaCola + colasRevisadas + 1) % listaColasPrioridad.size();
        else {
            noAsignados.add(proceso);
            fallosAsignacion++;
        }
            
        // Simulamos por último el tiempo de asignación porque si se interrumpe
        // ya hemos completado la operación de asignación
        tiempo = tiempoAsignacion(proceso.getTipoPrioridad());
        TimeUnit.SECONDS.sleep(tiempo);
    }
    
    private void finalizacion(Informe informe) {        
        informe.setListaColasPrioridad(listaColasPrioridad);
        informe.setListaNoAsignados(noAsignados);
    }
    
    public String getiD() {
        return iD;
    }

    private int tiempoAsignacion(TipoPrioridad tipoPrioridad) {
        return MINIMO + tipoPrioridad.ordinal();
    }
}
