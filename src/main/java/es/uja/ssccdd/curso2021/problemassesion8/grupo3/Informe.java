/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo3;

import java.util.ArrayList;

/**
 *
 * @author pedroj
 */
public class Informe {
    private final String gestor;
    ArrayList<UnidadAlmacenamiento> listaDeAlmacenamiento;
    ArrayList<Archivo> listaNoAsignados;

    public Informe(String gestor) {
        this.gestor = gestor;
        this.listaDeAlmacenamiento = null;
        this.listaNoAsignados = null;
    }

    public Informe(String gestor, ArrayList<UnidadAlmacenamiento> listaDeAlmacenamiento, ArrayList<Archivo> listaNoAsignados) {
        this.gestor = gestor;
        this.listaDeAlmacenamiento = listaDeAlmacenamiento;
        this.listaNoAsignados = listaNoAsignados;
    }

    public void setListaDeAlmacenamiento(ArrayList<UnidadAlmacenamiento> listaDeAlmacenamiento) {
        this.listaDeAlmacenamiento = listaDeAlmacenamiento;
    }

    public void setListaNoAsignados(ArrayList<Archivo> listaNoAsignados) {
        this.listaNoAsignados = listaNoAsignados;
    }

    @Override
    public String toString() {
        String resultado = "******************** " + gestor + " ********************\n";
        
        for(UnidadAlmacenamiento unidadAlmacenamiento : listaDeAlmacenamiento)
            resultado = resultado + unidadAlmacenamiento + "\n";
            
        resultado = resultado + "Archivos no asignados " + listaNoAsignados + "\n";
        
        return resultado;
    }
}
