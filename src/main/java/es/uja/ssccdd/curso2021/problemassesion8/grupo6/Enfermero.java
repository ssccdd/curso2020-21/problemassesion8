/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo6;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo6.Utils.TIEMPO_ESPERA_ENFERMERO;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.PriorityBlockingQueue;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Enfermero implements Runnable {

    private final int iD;
    private final PriorityBlockingQueue<Paciente> pacientes;
    private final BlockingDeque<DosisVacuna> dosis;

    public Enfermero(int iD, PriorityBlockingQueue<Paciente> pacientes, BlockingDeque<DosisVacuna> dosis) {
        this.iD = iD;
        this.pacientes = pacientes;
        this.dosis = dosis;
    }

    @Override
    public void run() {

        boolean interrumpido = false;
        ArrayList<Paciente> copiaLocalPacientes = new ArrayList<>();
        Paciente pacienteActual = null;

        System.out.println("Enfermero " + iD + " ha empezado.");

        while (!interrumpido) {

            try {

                if (pacienteActual == null) {
                    pacienteActual = pacientes.take();
                    copiaLocalPacientes.add(pacienteActual);
                }

                DosisVacuna siguienteDosis = dosis.take();

                boolean inyectado = pacienteActual.addDosisVacuna(siguienteDosis);
                if (inyectado) {
                    if (pacienteActual.isInmunizado()) {
                        //Si el paciente está inmunizado, en la siguiente vuelta se cogerá uno nuevo
                        pacienteActual = null;
                    }
                } else {
                    dosis.add(siguienteDosis);
                }

                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_ENFERMERO);

            } catch (InterruptedException ex) {
                interrumpido = true;
            }

        }

        imprimirInfo(copiaLocalPacientes);

    }

    /**
     * Imprime una cadena con la información de la clase
     *
     * @param trabajo pacientes inmunizados
     */
    public void imprimirInfo(ArrayList<Paciente> trabajo) {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\n\nEnfermero ").append(iD).append(", inmunizados ").append(trabajo.size()).append(" pacientes");

        for (Paciente pac : trabajo) {
            mensaje.append("\n\t").append(pac.toString());
        }

        System.out.println(mensaje.toString());
    }

}
