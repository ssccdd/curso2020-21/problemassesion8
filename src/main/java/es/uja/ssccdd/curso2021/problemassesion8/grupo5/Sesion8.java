/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo5;

import es.uja.ssccdd.curso2021.problemassesion8.grupo5.Utils.CalidadImpresion;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo5.Utils.INGENIEROS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo5.Utils.CLIENTES_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo5.Utils.MAQUINAS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo5.Utils.VALOR_GENERACION;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo5.Utils.random;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo5.Utils.TAMAÑO_COLA_PETICIONES;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo5.Utils.TIEMPO_ESPERA_HILO_PRINCIPAL;

import java.util.Iterator;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion8 {

    public static void main(String[] args) throws InterruptedException {

        ExecutorService executor = (ExecutorService) Executors.newCachedThreadPool();

        BlockingDeque<Modelo> peticiones = new LinkedBlockingDeque<>(TAMAÑO_COLA_PETICIONES);
        DelayQueue<Modelo> modelosImpresos = new DelayQueue<>();
        BlockingDeque<Modelo> modelosProcesados = new LinkedBlockingDeque<>();

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        System.out.println("HILO-Principal Generando clientes");
        for (int i = 0; i < CLIENTES_A_GENERAR; i++) {
            executor.execute(new Cliente(i, peticiones, CalidadImpresion.getCalidad(random.nextInt(VALOR_GENERACION))));
        }

        System.out.println("HILO-Principal Generando ingenieros");
        for (int i = 0; i < INGENIEROS_A_GENERAR; i++) {
            executor.execute(new Ingeniero(i, peticiones, modelosImpresos));
        }

        System.out.println("HILO-Principal Generando máquinas");
        for (int i = 0; i < MAQUINAS_A_GENERAR; i++) {
            executor.execute(new MaquinaPostProcesado(i, modelosImpresos, modelosProcesados));
        }

        System.out.println("HILO-Principal Espera para parar a los ingenieros");

        try {
            TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_HILO_PRINCIPAL);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion8.class.getName()).log(Level.SEVERE, null, ex);
        }

        executor.shutdownNow();

        try {
            executor.awaitTermination(TIEMPO_ESPERA_HILO_PRINCIPAL, TimeUnit.DAYS);
        } catch (InterruptedException ex) {
            //No es necesario tratar la excepción puesto que el hilo principal no se va a interrumpir.
            Logger.getLogger(Sesion8.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Ha finalizado la ejecución");

        System.out.println("\n\nHILO-Principal Listado " + peticiones.size() + " peticiones pendientes.");
        while (!peticiones.isEmpty()) {
            System.out.println(peticiones.poll());
        }

        System.out.println("\n\nHILO-Principal Listado " + modelosImpresos.size() + " modelos en impresión.");
        for (Iterator<Modelo> iterator = modelosImpresos.iterator(); iterator.hasNext();) {
            System.out.println(iterator.next());
        }

        System.out.println("\n\nHILO-Principal Listado " + modelosProcesados.size() + " modelos procesados.");
        while (!modelosProcesados.isEmpty()) {
            System.out.println(modelosProcesados.poll());
        }

    }

}
