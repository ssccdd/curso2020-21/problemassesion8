/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo5;

import es.uja.ssccdd.curso2021.problemassesion8.grupo5.Utils.CalidadImpresion;
import java.util.Date;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Modelo implements Delayed {

    private final int iD;
    private final CalidadImpresion calidadRequeridad;
    private Date horaTerminacion;

    public Modelo(int iD, CalidadImpresion calidadRequeridad) {
        this.iD = iD;
        this.calidadRequeridad = calidadRequeridad;
    }

    public Modelo(int iD, CalidadImpresion calidadRequeridad, Date horaTerminación) {
        this.iD = iD;
        this.calidadRequeridad = calidadRequeridad;
        this.horaTerminacion = horaTerminación;
    }

    public int getiD() {
        return iD;
    }

    public CalidadImpresion getCalidadRequeridad() {
        return calidadRequeridad;
    }

    public void setHoraTerminacion(Date horaTerminacion) {
        this.horaTerminacion = horaTerminacion;
    }

    /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        if (horaTerminacion != null) {
            Date now = new Date();
            long diff = horaTerminacion.getTime() - now.getTime();
            return "Modelo{" + "iD=" + iD + ", calidad requerida=" + calidadRequeridad + ", le quedan " + diff + " milisegundos}";
        } else {
            return "Modelo{" + "iD=" + iD + ", calidad requerida=" + calidadRequeridad + ", no encolado en impresora}";
        }
    }

    /**
     * Method to compare two events
     */
    @Override
    public int compareTo(Delayed o) {
        long result = this.getDelay(TimeUnit.NANOSECONDS) - o.getDelay(TimeUnit.NANOSECONDS);
        if (result < 0) {
            return -1;
        } else if (result > 0) {
            return 1;
        }
        return 0;
    }

    /**
     * Method that returns the remaining time to the activation of the event
     */
    @Override
    public long getDelay(TimeUnit unit) {
        Date now = new Date();
        long diff = horaTerminacion.getTime() - now.getTime();
        return unit.convert(diff, TimeUnit.MILLISECONDS);
    }

}
