/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo2;

import static es.uja.ssccdd.curso2021.problemassesion8.grupo2.Constantes.CAPACIDAD_PROCESOS;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo2.Constantes.INICIO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo2.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo2.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo2.Constantes.NUM_GESTORES;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo2.Constantes.TIEMPO_CREACION_PROCESO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo2.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo2.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Sesion8 {
    static int ID_SISTEMA = 0;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ScheduledExecutorService ejecucion;
        Informe informe;
        PriorityBlockingQueue<Proceso> procesosCreados;
        ArrayList<GestorProcesos> listaGestores;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicializamos las variables del sistema
        int numTareasCrearProceso = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
        ejecucion = Executors.newScheduledThreadPool(NUM_GESTORES + numTareasCrearProceso);
        listaGestores = new ArrayList();
        procesosCreados = new PriorityBlockingQueue(CAPACIDAD_PROCESOS);
        
        // Creamos los gestores
        for(int i = 0; i < NUM_GESTORES; i++) {
            GestorProcesos gestor = new GestorProcesos("GESTOR(" + i + ")", procesosCreados);
            listaGestores.add(gestor);
        }
        
        // Ejecutamos la tarea para crear proceos
        // Lo modifico para crear más de un proceso y que evolucione más rápido el ejercicio
        for(int i = 0; i < numTareasCrearProceso; i++) {
            CrearProceso tareaCrearProceso = new CrearProceso("CREAR_PROCESO(" + i + ")", procesosCreados);
            ejecucion.scheduleAtFixedRate(tareaCrearProceso, INICIO, TIEMPO_CREACION_PROCESO, TimeUnit.SECONDS);
            
        }
        
        // Ejecutamos los gestores y esperamos a que finalice el primero
        System.out.println("(HILO_PRINCIPAL) Espera a la finalización de un gestor");
        informe = ejecucion.invokeAny(listaGestores);
        
        // Se solicita la cancelación de los gestores que no han terminado
        System.out.println("(HILO_PRINCIPAL) Solicita la finalización de los gestores");
        ejecucion.shutdownNow();
        ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        
        // Mostrar el estado de sistema distribuido
        System.out.println("(HILO_PRINCIPAL) Estado del gestor que ha finalizado\n" +
                            informe);
        
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
    
    public static synchronized int generaID_SISTEMA() {
        return ID_SISTEMA++;
    }
}
