/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo4;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Cliente implements Runnable {

    private final int iD;
    private final BlockingDeque<MenuReparto> listaDeMenus;
    private static int siguienteID = 0;
    private static ReentrantLock lockID = new ReentrantLock();

    public Cliente(int iD, BlockingDeque<MenuReparto> listaDeMenus) {
        this.iD = iD;
        this.listaDeMenus = listaDeMenus;
    }

    private static int getSiguienteID() {
        int id = 0;
        lockID.lock();
        id = siguienteID++;
        lockID.unlock();
        return id;
    }

    @Override
    public void run() {

        System.out.println("Cliente " + iD + " ha empezado.");

        int idMenu = getSiguienteID();

        try {
            listaDeMenus.put(new MenuReparto(idMenu));
            System.out.println("Cliente " + iD + " ha terminado correctamente.");
        } catch (InterruptedException ex) {
            //Si nos interrumpen no hacemos nada, porque no hay bucle que parar.
            System.out.println("Cliente " + iD + " ha terminado por interrupción.");
        }

    }

}
