/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo4;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo4.Utils.TIEMPO_ESPERA_REPARTIDOR;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.DelayQueue;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Repartidor implements Runnable {

    private final int iD;
    private final BlockingDeque<MenuReparto> listaDeMenus;
    private final DelayQueue<Plato> listaDePlatos;

    public Repartidor(int iD, BlockingDeque<MenuReparto> listaDeMenus, DelayQueue<Plato> listaDePlatos) {
        this.iD = iD;
        this.listaDeMenus = listaDeMenus;
        this.listaDePlatos = listaDePlatos;
    }

    @Override
    public void run() {

        ArrayList<MenuReparto> listaLocalMenús = new ArrayList<>();
        boolean interrumpido = false;
        MenuReparto menuActual = null;
        System.out.println("Repartidor " + iD + " ha empezado.");

        while (!interrumpido) {

            try {
                //Si no tengo menú asignado
                if (menuActual == null) {
                    menuActual = listaDeMenus.take();
                    listaLocalMenús.add(menuActual);
                }

                Plato plato = listaDePlatos.take();

                boolean insertado = menuActual.addPlato(plato);

                if (insertado) {
                    if (menuActual.pedidoCompleto()) {
                        //Si el menú está completo, en la siguiente vuelta se cogerá uno nuevo
                        menuActual = null;
                    }
                } else {
                    listaDePlatos.add(plato);
                }

                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_REPARTIDOR);
            } catch (InterruptedException ex) {
                interrumpido = true;
            }

        }

        imprimirDatos(listaLocalMenús);

    }

    /**
     * Imprimr la información del hilo
     *
     * @param trabajoHecho menús rellenos
     */
    private void imprimirDatos(ArrayList<MenuReparto> trabajoHecho) {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\nRepartidor ").append(iD).append(" rellenados ").append(trabajoHecho.size()).append(" menús.");

        for (MenuReparto pedido : trabajoHecho) {
            mensaje.append("\n\t").append(pedido.toString());
        }

        mensaje.append("\n");

        System.out.println(mensaje.toString());
    }

}
