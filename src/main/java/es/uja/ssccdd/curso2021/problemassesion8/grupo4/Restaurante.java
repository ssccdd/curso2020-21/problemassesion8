/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo4;

import es.uja.ssccdd.curso2021.problemassesion8.grupo4.Utils.TipoPlato;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo4.Utils.PLATOS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo4.Utils.random;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo4.Utils.DELAY_PLATO_MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo4.Utils.DELAY_PLATO_MAXIMO;
import java.util.Date;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Restaurante implements Runnable {

    private final int iD;
    private final DelayQueue<Plato> listaDePlatos;
    private static int siguienteID = 0;
    private static ReentrantLock lockID = new ReentrantLock();

    public Restaurante(int iD, DelayQueue<Plato> listaDePlatos) {
        this.iD = iD;
        this.listaDePlatos = listaDePlatos;
    }

    private static int getSiguienteID() {
        int id = 0;
        lockID.lock();
        id = siguienteID++;
        lockID.unlock();
        return id;
    }

    private static Date getRandomDelay(int minSec, int maxSec) {

        int rand = random.nextInt(maxSec - minSec) + minSec;
        Date now = new Date();
        Date delay = new Date();
        delay.setTime(now.getTime() + (rand * 1000));

        return delay;

    }

    @Override
    public void run() {

        System.out.println("Restaurante " + iD + " ha empezado.");

        for (int i = 0; i < PLATOS_A_GENERAR; i++) {

            int idPlato = getSiguienteID();

            listaDePlatos.add(new Plato(idPlato, TipoPlato.getPlatoAleatorio(random.nextInt(Utils.VALOR_GENERACION)), getRandomDelay(DELAY_PLATO_MINIMO, DELAY_PLATO_MAXIMO)));

        }

        System.out.println("Restaurante " + iD + " ha terminado.");

    }

}
