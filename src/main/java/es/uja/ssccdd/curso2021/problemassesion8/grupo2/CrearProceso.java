/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo2;

import static es.uja.ssccdd.curso2021.problemassesion8.grupo2.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo2.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo2.Constantes.VALOR_CONSTRUCCION;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo2.Constantes.aleatorio;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo2.Sesion8.generaID_SISTEMA;
import static es.uja.ssccdd.curso2021.problemassesion8.grupo2.Constantes.TipoPrioridad.getTipoPrioridad;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class CrearProceso implements Runnable {
    private final String iD;
    private final BlockingQueue<Proceso> procesosCreados;

    public CrearProceso(String iD, BlockingQueue<Proceso> procesosCreados) {
        this.iD = iD;
        this.procesosCreados = procesosCreados;
    }
    
    @Override
    public void run() {
        try {
            
            crearProceso();
            
            System.out.println("TAREA-" + iD + " Crea un proceso");
            
        } catch (InterruptedException ex) {
            System.out.println("TAREA-" + iD + " Ha sido CANCELADO " + ex);
        } 
    }
    
    private void crearProceso() throws InterruptedException {
        // Simula la creación de un proceso
        int construccion = aleatorio.nextInt(VALOR_CONSTRUCCION);
        Proceso proceso = new Proceso(generaID_SISTEMA(), getTipoPrioridad(construccion));
        int tiempo = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
        TimeUnit.SECONDS.sleep(tiempo);
        
        // Se añade el proceso a la lista de procesos creados
        procesosCreados.put(proceso);
    }

    public String getiD() {
        return iD;
    }    
}
