/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion8.grupo4;

import java.util.Random;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Utils {

    public static Random random = new Random();

    // Constantes del problema
    public static final int VALOR_GENERACION = 101; // Valor máximo
    public static final int TOTAL_TIPOS_PLATOS = TipoPlato.values().length;
    public static final int TIEMPO_ESPERA_REPARTIDOR = 100;
    public static final int TIEMPO_ESPERA_HILO_PRINCIPAL = 30000;
    public static final int TAMAÑO_COLA_MENUS = 10;
    public static final int CLIENTES_A_GENERAR = 100;
    public static final int RESTAURANTES_A_GENERAR = 5;
    public static final int REPARTIDORES_A_GENERAR = 10;
    public static final int PLATOS_A_GENERAR = 100;
    public static final int DELAY_PLATO_MINIMO = 1;
    public static final int DELAY_PLATO_MAXIMO = 15;

    //Enumerado para el tipo de plato
    public enum TipoPlato {
        PRINCIPAL(33), SEGUNDO(66), POSTRE(100);

        private final int valor;

        private TipoPlato(int valor) {
            this.valor = valor;
        }

        /**
         * Obtenemos un plato relacionado con su valor de generación
         *
         * @param valor, entre 0 y 100, de generación del plato
         * @return el TipoPlato con el valor de generación
         */
        public static TipoPlato getPlatoAleatorio(int valor) {
            TipoPlato resultado = null;
            TipoPlato[] platos = TipoPlato.values();
            int i = 0;

            while ((i < platos.length) && (resultado == null)) {
                if (platos[i].valor >= valor) {
                    resultado = platos[i];
                }

                i++;
            }

            return resultado;
        }

        /**
         * Obtenemos un plato relacionado con su valor de generación
         *
         * @param ordinal, entre 0 y TOTAL_TIPO_PLATOS - 1
         * @return el TipoPlato con el valor de generación
         */
        public static TipoPlato getPlatoOrdinal(int ordinal) {
            return TipoPlato.values()[ordinal];
        }
    }

}
