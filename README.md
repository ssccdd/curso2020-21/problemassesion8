[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 8

Problemas propuestos para la Sesión 8 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2020-21.

En esta sesión de prácticas se proponen ejercicio para trabajar con el paquete [java.util.concurrent](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/package-frame.html). En los ejercicios nos centraremos en algunas clases que implementan la interface [`BlockingQueue`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/BlockingQueue.html "interface in java.util.concurrent") que permite acceder a los elementos de una estructura de datos de forma segura por los diferentes hilos. Seguimos utilizando la interface [`ExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ExecutorService.html "interface in java.util.concurrent") para la ejecución de las tareas. Los ejercicios son diferentes para cada grupo:

- [Grupo 1](https://gitlab.com/ssccdd/curso2020-21/problemassesion8#grupo-1)
- [Grupo 2](https://gitlab.com/ssccdd/curso2020-21/problemassesion8#grupo-2)
- [Grupo 3](https://gitlab.com/ssccdd/curso2020-21/problemassesion8#grupo-3)
- [Grupo 4](https://gitlab.com/ssccdd/curso2020-21/problemassesion8#grupo-4)
- [Grupo 5](https://gitlab.com/ssccdd/curso2020-21/problemassesion8#grupo-5)
- [Grupo 6](https://gitlab.com/ssccdd/curso2020-21/problemassesion8#grupo-6)

### Grupo 1
En el ejercicio utilizaremos diferentes tareas y las posibilidades de la *ejecución planificada*. Las herramientas que utilizaremos para la solución del ejercicio serán: [`ExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ExecutorService.html "interface in java.util.concurrent") para la ejecución de las tareas y la clase [`LinkedBlockingDeque`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/LinkedBlockingDeque.html) para las estructuras de datos necesarias. Para la realización del ejercicio se hará uso de las clases ya definidas: `Proceso`, `Ordenador`. Además en la interface `Constantes` también deberá utilizarse en el desarrollo del ejercicio. 

Hay que completar la implementación de las siguientes clases:

- `CrearProcesos` : Esta tarea se encargará de ir creando un proceso y tiene como variables de instancia:
	 - Un identificador.
	 - Un `TipoProceso` asociado al proceso que crea.
	 - Una `LinkedBlockingDeque` para almacenar los procesos que se crean.

	La tarea que tiene que realizar es:
	- Crea un proceso
	- Se simulará un tiempo de creación definido por las constantes.
	- Se almacena el proceso creado en la lista.
	- Debe programarse la interrupción de la tarea de creación.

- `GestorMemoria` : Es el encargado de asignar un `Proceso` a un ordenador de los que tiene asignados. La clase tiene las siguientes variables de instancia:
	-  Un identificador
	- La `LinkedBlockingDeque` donde están almacenados los procesos que se han creado.

	La tarea que debe realizar es la siguiente:
	- Crea una lista de ordenadores a la que tendrá que asignar los procesos.
	- Crea una lista para añadir los procesos no asignados.
	- Mientras no se alcance el número de fallos establecido:
		- Se obtiene el primer proceso almacenado en la lista.
		- Para cada `Proceso` se buscará un ordenador donde asignarlo, hay que procurar que la carga de procesos esté balanceada entre los diferentes ordenadores. Se simulará un tiempo aleatorio, atendiendo al tipo de proceso, para la realización de esta operación.
		- Si no ha podido ser asignado el proceso se añadirá a la lista de procesos no asignados. Esto cuenta como un fallo.
	- Programar la interrupción completando la última asignación de proceso.
	- El resultado de la ejecución de esta tarea es crear un `Informe` donde se debe mostrar el estado de asignación de procesos a los ordenadores y los procesos no asignados.

- `Informe` : Es el resultado de la ejecución de una tarea `GestorMemoria`.

- `Hilo Principal` Deberá completar los siguientes pasos:
	- Se añade a un ejecutor planificado la tarea para crear procesos que deberá ejecutarse cíclicamente por un tiempo establecido en las constantes.
	- Crea un número de gestores de memoria que estarán determinados por una constante.
	- Añadir los gestores al marco de ejecución y esperar a que finalice uno.
	- Solicita la interrupción de todas las tareas que queden activas.
	- Finaliza el marco de ejecución.
	- Presentará el informe del gestor que ha finalizado.

### Grupo 2
En el ejercicio utilizaremos diferentes tareas y las posibilidades de la *ejecución planificada*. Las herramientas que utilizaremos para la solución del ejercicio serán: [`ExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ExecutorService.html "interface in java.util.concurrent") para la ejecución de las tareas y las clases [`PriorityBlockingQueue`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/PriorityBlockingQueue.html "class in java.util.concurrent") y [`LinkedBlockingDeque`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/LinkedBlockingDeque.html) para las estructuras de datos necesarias. Para la realización del ejercicio se hará uso de las clases ya definidas: `Proceso`, `ColaPrioridad`. Además en la interface `Constantes` también deberá utilizarse en el desarrollo del ejercicio. 

Hay que completar la implementación de las siguientes clases:

- `Proceso` : Completar el método `compareTo(..)`.

 - `CrearProcesos` : Esta tarea se encargará de ir creando procesos y tiene las siguientes variables de instancia:
	 - Un identificador.
	 - Una `PriorityBlockingQueue` para que los procesos se almacenen según su prioridad.

	La tarea que tiene que realizar es:
	 - Se crea un `Proceso` con un tipo de prioridad aleatorio y se simulará un tiempo de creación utilizando las constantes.
	 - Se debe programar la interrupción de la tarea si es solicitada.
	 - Se almacena el proceso en la lista.

- `GestorProcesos` : Es el encargado de asignar un `Proceso` a una cola de prioridad. La clase tiene las siguientes variables de instancia:
	- Un identificador
	- La `PriorityBlockingQueue` donde están almacenados los procesos según su prioridad.

	La tarea que debe realizar es la siguiente:
	- Crear una lista de colas de prioridad a las que tiene acceso el gestor.
	- Crear una lista de procesos no asignados.
	- Mientras no se alcance el número de fallos establecido.
		- Se obtiene el primer proceso de la lista
		- Para cada uno de los procesos se buscará una cola de prioridad donde asignarlo, hay que procurar que la carga de procesos esté balanceada. Se simulará un tiempo aleatorio para la realización de esta operación mediante las constantes.
		- Si no ha podido ser asignado el proceso se añadirá a la lista de procesos no asignados. Esto cuenta como un fallo.
	- Programar la interrupción completando la última asignación de proceso.
	- El resultado de la tarea es el informe del estado de sus colas de prioridad y los procesos no asignados. 

- `Hilo Principal` Deberá completar los siguientes pasos:
	- Se añade a un ejecutor planificado la tarea para crear procesos que deberá ejecutarse cíclicamente por un tiempo establecido en las constantes.
	- Crea un número de gestores que estarán determinados por una constante.
	- Añadir los gestores al marco de ejecución y esperar a que finalice uno.
	- Solicita la interrupción del resto de tareas activas.
	- Finaliza el marco de ejecución.
	- Presentará el informe del gestor que ha finalizado.

### Grupo 3
En el ejercicio utilizaremos diferentes tareas y las posibilidades de la *ejecución planificada*. Las herramientas que utilizaremos para la solución del ejercicio serán: [`ExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ExecutorService.html "interface in java.util.concurrent") para la ejecución de las tareas y las clases [`DelayQueue`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/DelayQueue.html "class in java.util.concurrent") y [`LinkedBlockingDeque`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/LinkedBlockingDeque.html) para las estructuras de datos necesarias. Para la realización del ejercicio se hará uso de las clases ya definidas: `Archivo`, `UnidadAlmacenamiento`. Además en la interface `Constantes` también deberá utilizarse en el desarrollo del ejercicio. 

Hay que completar la implementación de las siguientes clases:

- `Archivo` : Hay que completar los métodos `getDelay(..)` y `compareTo(..)`.

 - `CrearArchivoss` : Esta tarea se encargará de ir creando un archivo y tiene como variable de instancia:
	 - Un identificador.
	 - Una `DelayQueue` donde se almacenarán los archivos con un tiempo de activación asociado a su tipo de archivo.

	La tarea que tiene que realizar es:
	- Crea un archivo.
	- Se simulará un tiempo de creación definido por las constantes.
	- Se almacena el archivo en la lista.
	- Debe programarse la interrupción de la tarea de creación.

- `GestorAlmacenamiento` : Es el encargado de asignar un `Archivo` a una unidad de almacenamiento que tiene asignadas. La clase tiene las siguientes variables de instancia:
	- Un identificador
	- La `DelayQueue` donde están almacenados los archivos que se han creado.

	La tarea que debe realizar es la siguiente:
	- Crea una lista de unidades de almacenamiento a la que tendrá que asignar los archivos.
	- Crea una lista para añadir los archivos no asignados.
	- Mientras no se alcance un número de archivos almacenados establecido:
		- Se obtiene el primer archivo disponible en la lista.
		- Ciclo de asignación de archivos hasta que se haya alcanzado el máximo de archivos establecido:
			- Para cada `Archivo` se buscará la primera unidad de almacenamiento donde asignarlo. Se simulará un tiempo aleatorio, atendiendo al tipo de archivo, para la realización de esta operación.
			- Si no ha podido ser asignado el archivo se añadirá a la lista de archivos no asignados.
	- Programar la interrupción completando la última asignación de archivo.
	- El resultado de la ejecución de esta tarea es crear un `Informe` donde se debe mostrar el estado de asignación de los archivos y los archivos no asignados.

- `Informe` : Es el resultado de la ejecución de una tarea `GestorAlmacenamiento`.

- `Hilo Principal` Deberá completar los siguientes pasos:
	- Se añade a un ejecutor planificado la tarea para crear archivos que deberá ejecutarse cíclicamente por un tiempo establecido en las constantes.
	- Crea un número de gestores de almacenamiento que estarán determinados por una constante.
	- Añadir los gestores al marco de ejecución y esperar a que finalice uno.
	- Solicita la interrupción del de tareas activas.
	- Finaliza el marco de ejecución.
	- Presentará el informe del gestor que ha finalizado.

### Grupo 4
Se ha decidido que la forma anterior de trabajar con la gestión de los menús no era eficiente. Para optimizar recursos, los `Restaurantes` generarán todos los `Platos` necesarios, aunque los platos salen muy calientes y no se pueden organizar hasta que baje su temperatura, esto se simulará con una [`DelayQueue`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/DelayQueue.html). En esta sesión se añadirá la clase `Cliente`, cada cliente generará una peticion de un `MenuReparto` en una [`LinkedBlockingDeque`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/LinkedBlockingDeque.html). Para que posteriormente los `repartidores` puedan recoger esas peticiones y vayan rellenándolas con los platos. Se usarán marcos de ejecución para controlar la **creación/ejecución/sincronización** de los hilos, en concreto alguna implementación de  `Executor` que permita generar tantos hilos como sea necesario.

- `Utils`: Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay constantes no incluidas que se deben completar.
- `Plato`: Clase que representa a un plato de tipo `TipoPlato`. Implementa la interfaz Delayed, es necesario rellenar los métodos asociados.
- `MenuReparto`: Representa a un pedido, puede incluir un plato de cada uno de los presentes en `TipoPlato`, pero no más de uno de cada tipo.
- Las clases que lo necesiten tendrán ya incluido métodos protegidos para generar ids únicos y métodos para generar retrasos aleatorios en un rango concreto.

El ejercicio consiste en completar la implementación de las siguientes clases:
- `Cliente`:
	- Los clientes guardarán en variables de instancia, su identificador y la `LinkedBlockingDeque` de menús.
	- Los clientes generarán un pedido vacío con ID único, y lo insertará en la `LinkedBlockingDeque`.
	- No es necesario controlar la interrupción, si un cliente es interrumpido terminará su ejecución.
	- Al terminar, el cliente indicará que ha terminado y si ha sido interrumpido.
- `Restaurante`:
	- Los restaurantes guardarán en variables de instancia, su identificador y la `DelayQueue` de platos.
	- Cada restaurante fabricará 100 platos, cada uno de tipo aleatorio y con un delay aleatorio entre 1 y 15 segundos.
	- El restaurante no esperará entre platos.
- `Repartidor`:
	- El repartidor guardará en variables de instancia, su identificador, una `LinkedBlockingDeque` de pedidos y una `DelayQueue` de platos.
	- El repartidor estará ejecutando un bucle hasta ser interrumpido:
		- Cuando no tenga un menú asignado o haya completado el último, cogerá uno de la cola de pedidos.
		- Cogerá un plato de la cola de platos, si ese plato no se puede insertar en el menú, lo volverá a insertar en la cola de nuevo.
		- Independientemente de si ha podido insertar el plato o no, el repartidos esperará 100 milisegundos por cada plato cogido de la cola.
	- Al recibir la interrupción saldrá del bucle sin acabar el pedido e imprimirá los datos de los pedidos que ha procesado, incluyendo al último aunque no este completo.
- `Hilo Principal`:
	- Generará una `DelayQueue` para los platos y una `LinkedBlockingDeque` limitada a 10 pedidos.
	- Creará e iniciará la ejecución de 5 restaurantes usando un gestor de hilos.
	- Creará e iniciará la ejecución de 100 clientes usando un gestor de hilos.
	- Creará e iniciará la ejecución de 10 repartidores usando un gestor de hilos.
	- Esperará 30 segundos antes de hacer un `shutdownNow()` del gestor de hilos para interrumpir a los repartidores y clientes, y se bloqueará hasta que acaben con `awaitTermination()`.
	- Al finalizar, imprimirá el contenido de ambas colas.

### Grupo 5
Se ha decidido que la forma anterior de trabajar con la gestión de la impresión y postprocesado de modelos no era eficiente. Para optimizar el tiempo, primero los `Clientes` generan peticiones de modelos, para que posteriormente los `Ingenieros` los manden a imprimir asignándoles un tiempo, pasado ese tiempo las `MáquinaPostprocesado` reciben y procesan los modelos. Para simular este comportamiento se usará una [`LinkedBlockingDeque`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/LinkedBlockingDeque.html) de modelos para las peticiones, una [`DelayQueue`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/DelayQueue.html) para los modelos en impresión y una [`LinkedBlockingDeque`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/LinkedBlockingDeque.html) de modelos procesados.  Se usarán marcos de ejecución para controlar la **creación/ejecución/sincronización** de los hilos, en concreto alguna implementación de `Executor` que permita generar tantos hilos como sea necesario.

- `Utils`: Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay constantes no incluidas que se deben completar.
- `Modelo`: Clase que representa a un modelo 3D con unos requisitos de tipo `CalidadImpresion`. Implementa la interfaz Delayed, es necesario rellenar los métodos asociados.
- Las clases que lo necesiten tendrán ya incluido métodos protegidos para generar ids únicos y métodos para generar retrasos aleatorios en un rango concreto.

El ejercicio consiste en completar la implementación de las siguientes clases:
 - `Cliente`:
	- Los clientes guardarán en variables de instancia, su identificador, la `LinkedBlockingDeque` de peticiones y una calidad para generar modelos.
	- En un bucle continuo:
		- Generará un pedido de modelo con ID único, y su mismo tipo de calidad y lo insertará en la `LinkedBlockingDeque`.
		- Esperará 500 milisegundos entre cada petición.
	- No es necesario controlar la interrupción, si un cliente es interrumpido terminará su ejecución.
	- Al terminar, el cliente indicará que ha sido interrumpido y cuantas peticiones ha realizado.
- `Ingeniero`:
	- Los ingenieros guardarán en variables de instancia, su identificador, la `LinkedBlockingDeque` de peticiones y la `DelayQueue` de modelos en impresión.
	- En un bucle continuo:
		- Cogerá un modelo de la lista de peticiones, le asignará un tiempo de espera entre 3 y 9 segundos antes de insertar el modelo en la lista de impresiones.
		- Después de crear el modelo el ingeniero esperará 450 milisegundos.	
	- No es necesario controlar la interrupción, si un ingeniero es interrumpido terminará su ejecución.
	- Al terminar, el ingeniero indicará que ha sido interrumpido y cuantas peticiones ha atendido.
 - `MaquinaPostprocesado`:
   	-  Las máquinas guardarán en variables de instancia, su identificador, la `DelayQueue` de modelos en impresión y la `LinkedBlockingDeque` de modelos procesados. 
   	- En un bucle continuo:
   		- Cogerá un modelo de la cola de modelos en impresión y lo introducira en la cola de modelos procesados.
   		- Esperará 350 milisegundos entre cada petición.
	- No es necesario controlar la interrupción, si una máquina es interrumpida terminará su ejecución.
	- Al terminar, la máquina indicará que ha sido interrumpida y cuantos modelos ha procesado.
- `Hilo Principal`:
	- Generará una `LinkedBlockingDeque` limitada a 20 peticiones, una `DelayQueue` para los modelos en impresión y una `LinkedBlockingDeque` para los modelos procesados.
	- Creará e iniciará la ejecución de 15 clientes usando un gestor de hilos.
	- Creará e iniciará la ejecución de 10 ingenieros usando un gestor de hilos.
	- Creará e iniciará la ejecución de 5 máquinas usando un gestor de hilos.
	- Esperará 15 segundos antes de hacer un `shutdownNow()` del gestor de hilos para interrumpir a los clientes, ingenieros y máquinas, y se bloqueará hasta que acaben con `awaitTermination()`.
	- Al finalizar, imprimirá el contenido de las tres colas.

### Grupo 6
Se ha decidido que la forma anterior de trabajar con la gestión de la vacunación no era eficiente. Para optimizar el tiempo, una serie de `Centros médicos` derivará pacientes usando una [`PriorityBlockingQueue`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/PriorityBlockingQueue.html) ordenada de forma inversa por edad, los `Almacenes médicos` insertarán `DosisVacuna` en una [`LinkedBlockingDeque`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/LinkedBlockingDeque.html), para que posteriormente los `enfermeros` puedan vacunar a los pacientes. Se usarán marcos de ejecución para controlar la **creación/ejecución/sincronización** de los hilos, en concreto alguna implementación de `Executor` que permita generar tantos hilos como sea necesario.

- `Utils`: Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay constantes no incluidas que se deben completar.
- `DosisVacuna`: Clase que representa a cada dosis de la vacuna de un `FabricanteVacuna` determinado.
- `Paciente`: Representa a un paciente al que se le pueden administrar hasta dos dosis siempre que las dos sean del mismo fabricante. Implementa la interfaz Comparable, es necesario rellenar el método asociado.
- Las clases que lo necesiten tendrán ya incluido métodos protegidos para generar ids únicos y métodos para generar retrasos aleatorios en un rango concreto.

El ejercicio consiste en completar la implementación de las siguientes clases:
- `CentroMedico`:
	- Los centros guardarán en variables de instancia, su identificador y la `PriorityBlockingQueue` de pacientes.
	- En un bucle continuo:
		- Generará un paciente con ID único, de entre 15 y 100 años, y lo insertará en la `PriorityBlockingQueue`.
		- Esperará 200 milisegundos entre cada petición.
	- No es necesario controlar la interrupción, si un centro es interrumpido terminará su ejecución.
	- Al terminar, el centro indicará que ha sido interrumpido y cuantos pacientes ha derivado.
- `AlmacénMédico`:
	- Los almacenes guardarán en variables de instancia, su identificador y la `LinkedBlockingDeque` de dosis.
	- En un bucle continuo:
		- Generará una dosis con ID único, de una laboratorio aleatorio, y lo insertará en la `LinkedBlockingDeque`.
		- Esperará 90 milisegundos entre cada petición.
	- No es necesario controlar la interrupción, si un almacén es interrumpido terminará su ejecución.
	- Al terminar, el almacén indicará que ha sido interrumpido y cuantas dosis ha generado.
- `Enfermero`:
	- El enfermero guardará en variables de instancia, su identificador, la `PriorityBlockingQueue` de pacientes y la `LinkedBlockingDeque` de dosis.
	- El enfermero estará ejecutando un bucle hasta ser interrumpido:
		- Cuando no tenga un paciente asignado o haya inmunizado el último, cogerá uno de la cola de pacientes.
		- Cogerá una dosis de la cola de dosis, si esa dosis no se puede inyectar en el paciente, lo volverá a insertar en la cola de nuevo.
		- Independientemente de si ha podido inyectar la dosis o no, el enfermero esperará 100 milisegundos por cada dosis cogida.
	- Al recibir la interrupción saldrá del bucle sin acabar con el paciente e imprimirá los datos de los pacientes que ha procesado, incluyendo al último aunque no este inmunizado.
- `Hilo Principal`:
	- Generará una `PriorityBlockingQueue` para los pacientes y una `LinkedBlockingDeque` para las dosis.
	- Creará e iniciará la ejecución de 5 centros médicos usando un gestor de hilos.
	- Creará e iniciará la ejecución de 5 almacénes médicos usando un gestor de hilos.
	- Creará e iniciará la ejecución de 10 enfermeros usando un gestor de hilos.
	- Esperará 10 segundos antes de hacer un `shutdownNow()` del gestor de hilos para interrumpir a los centros, almacenes y enfermeros, y se bloqueará hasta que acaben con `awaitTermination()`.
	- Al finalizar, imprimirá el contenido de ambas colas.
